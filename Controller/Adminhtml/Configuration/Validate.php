<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Controller\Adminhtml\Configuration;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syte\ProductFeed\Model\Helper;
use Syte\ProductFeed\Model\Ftp as FtpHelper;

class Validate extends Action
{
    /** @const string */
    public const ADMIN_RESOURCE = 'Magento_Config::config';

    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var FtpHelper
     */
    private $ftpHelper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Action\Context $context
     * @param Helper $helper
     * @param FtpHelper $ftpHelper
     * @param @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Action\Context $context,
        Helper $helper,
        FtpHelper $ftpHelper,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->ftpHelper = $ftpHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        if ('runnow' === $this->getRequest()->getParam('action_mode')) {
            $response = $this->executeScheduleRunNow();
        } else {
            $response = $this->executeFtpValidation();
        }

        return $response;
    }

    /**
     * @return ResultInterface
     */
    public function executeFtpValidation(): ResultInterface
    {
        $storeId = (int)$this->storeManager->getStore()->getId();
        $data = [
            'is_ftp' => true,
            'host' => $this->getRequest()->getParam('ftp_host'),
            'port' => $this->getRequest()->getParam('ftp_port'),
            'user' => $this->getRequest()->getParam('ftp_user'),
            'password' => $this->getRequest()->getParam('ftp_password'),
            'path' => $this->getRequest()->getParam('ftp_path'),
        ];
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response->setHttpResponseCode(200);
        $validatedMsg = $this->ftpHelper->executeConnection($storeId, $data);
        $response->setData([
            'validated' => $validatedMsg ? false : true,
            'errormsg' => __($validatedMsg),
        ]);

        return $response;
    }

    /**
     * @return ResultInterface
     */
    public function executeScheduleRunNow(): ResultInterface
    {
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response->setHttpResponseCode(200);
        $successMsg = [];
        $errorMsg = [];
        if ($this->helper->createAndSendProductFeeds($successMsg, $errorMsg)) {
            $statusMsg = (empty($errorMsg) && !empty($successMsg)) ? __('A job has been completed successfully.')
                : __('Errors have been detected.');
             $statusMsg .=  ' ' . __('See logs for details.');
        } else {
            $statusMsg = __('This service is not available');
        }
        $response->setData([
            'executed' => true,
            'statusmsg' => $statusMsg,
        ]);

        return $response;
    }
}
