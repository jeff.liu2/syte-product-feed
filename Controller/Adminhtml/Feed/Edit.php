<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Controller\Adminhtml\Feed;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use Syte\ProductFeed\Model\FeedFactory;

class Edit extends Action
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var FeedFactory
     */
    private $feedFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param FeedFactory $feedFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        FeedFactory $feedFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->feedFactory = $feedFactory;
        parent::__construct($context);
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Syte_ProductFeed::edit');
    }

    /**
     * Initialization
     *
     * @return resultPage
     */
    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Syte_ProductFeed::history')
            ->addBreadcrumb(__('History'), __('History'))
            ->addBreadcrumb(__('Manage History Records'), __('Manage History Records'));

        return $resultPage;
    }

    /**
     * Executable
     *
     * @return resultPage
     */
    public function execute()
    {
        $model = $this->feedFactory->create();
        if ($id = $this->getRequest()->getParam('feed_id')) {
            $model->load($id);
            if (!$model->getFeedId()) {
                $this->messageManager->addError(__('These records no longer exist'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->coreRegistry->register('syteproductfeed_feed', $model);
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Record') : __('Add Record'),
            $id ? __('Edit Record') : __('Add Record')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Record'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getFeedId() ? (__('Product Feed History') . ' #' . $model->getFeedId())
                : __('Add Product Feed History'));

        return $resultPage;
    }
}
