<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model;

use Syte\ProductFeed\Api\Data\FeedInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Feed extends AbstractModel implements FeedInterface, IdentityInterface
{

    /** @const string */
    private const CACHE_TAG = 'syte_productfeed';

    //Unique identifier for use within caching
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'syte_feed';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Syte\ProductFeed\Model\ResourceModel\Feed::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getFeedId()];
    }

    /**
     * Get default values
     *
     * @return array
     */
    public function getDefaultValues(): array
    {
        $values = [];

        return $values;
    }

    /**
     * Get feed_id
     *
     * @return int
     */
    public function getFeedId(): int
    {
        return (int)parent::getData(self::FEED_ID);
    }

    /**
     * Get store_id
     *
     * @return int
     */
    public function getStoreId(): int
    {
        return (int)$this->getData(self::STORE_ID);
    }

     /**
      * Set store_id
      *
      * @param int $storeid
      *
      * @return $this
      */
    public function setStoreId(int $storeid)
    {
        return $this->setData(self::STORE_ID, $storeid);
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename(): string
    {
        return (string)$this->getData(self::FILENAME);
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return $this
     */
    public function setFilename(string $filename)
    {
        return $this->setData(self::FILENAME, $filename);
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus(): string
    {
        return (string)$this->getData(self::STATUS);
    }

     /**
      * Set status
      *
      * @param string $status
      *
      * @return $this
      */
    public function setStatus(string $status)
    {
        return $this->setData(self::STATUS, $status);
    }

     /**
      * Get job_started_at
      *
      * @return string
      */
    public function getJobStartedAt(): string
    {
        return (string)$this->getData(self::JOB_STARTED_AT);
    }

     /**
      * Set job_started_at
      *
      * @param string $jobStartedAt
      *
      * @return $this
      */
    public function setJobStartedAt(string $jobStartedAt)
    {
        return $this->setData(self::JOB_STARTED_AT, $jobStartedAt);
    }

    /**
     * Get created_at
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return (string)$this->getData(self::CREATED_AT);
    }

    /**
     * Get updated_at
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return (string)$this->getData(self::UPDATED_AT);
    }
}
