<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Syte\Core\Model\Constants;

class Connection implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => Constants::SYTE_CONFIG_CONNECTION_NONE, 'label' => __('None')],
            ['value' => Constants::SYTE_CONFIG_CONNECTION_SFTP, 'label' => __('SFTP')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            Constants::SYTE_CONFIG_CONNECTION_NONE => __('None'),
            Constants::SYTE_CONFIG_CONNECTION_SFTP => __('SFTP'),
        ];
    }
}
