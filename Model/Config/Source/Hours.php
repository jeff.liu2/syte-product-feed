<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Syte\Core\Model\Constants;

class Hours implements ArrayInterface
{
    /** const int */
    private const HOURS_MAX = 24;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $result = [];
        for ($h = 1; $h <= self::HOURS_MAX; $h++) {
            $result[] = ['value' => $h, 'label' => $h];
        }

        $result[] = ['value' => 48, 'label' => 48];
        $result[] = ['value' => 72, 'label' => 72];
        $result[] = ['value' => 96, 'label' => 96];

        return $result;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        for ($h = 1; $h <= self::HOURS_MAX; $h++) {
            $result[$h] = $h;
        }

        $result[48] = 48;
        $result[72] = 72;
        $result[96] = 96;

        return $result;
    }
}
