<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml\Form\Field\Ftp;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

class Validation extends Field
{
    /**
     * @inheritDoc
     */
    protected function _renderScopeLabel(AbstractElement $element): string
    {
        // Return empty label
        return '';
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        // Replace field markup with validation button
        $title = __('Test Connection');
        $endpoint = $this->getUrl('syteproductfeed/configuration/validate');
        // @codingStandardsIgnoreStart
        $html = <<<TEXT
            <button
                    type="button"
                    name="syte-pf-ftp-validation"
                    id="syte-pf-ftp-validation"
                    title="{$title}"
                    class="button syte-ftp-validation-button"
                    onclick="syteFtpValidator.call(this, '{$endpoint}')">
                    <span>{$title}</span>
            </button>
TEXT;
        // @codingStandardsIgnoreEnd

        return $html;
    }
}
